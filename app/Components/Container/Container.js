import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView,AppRegistry, TouchableHighlight} from 'react-native';

import BootstrapStyleSheet from 'react-native-bootstrap-styles';

const
  BODY_COLOR = '#000022',
  TEXT_MUTED = '#888888';

// custom constants
const constants = {
  BODY_COLOR, TEXT_MUTED,
};

// custom classes
const classes = {
  title: {
    color: 'red',
  }
};

const bootstrapStyleSheet = new BootstrapStyleSheet(constants, classes);
const s = styles = bootstrapStyleSheet.create();
const c = constants = bootstrapStyleSheet.constants;


export default class Container extends Component {
  render() {
    return (
          <View style={styles.container}>
            <Text style={styles.label}>Código del Ticket: 123456789</Text>
            <Text style={styles.label}>Comentario: Comenatrio de ejem...</Text>

            <TouchableHighlight onPress={this.onPress} style={[s.btnTouchable]}>
              <View style={[s.btn, s.btnPrimary]}>
                <Text style={[s.btnText, s.btnTextPrimary]}>Signup</Text>
              </View>
            </TouchableHighlight>
          </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'powderblue',
    width: 350,
    height: 150,
    borderRadius: 8,
    marginTop: 20,
  },

  label: {
    textAlign: 'center',
    color: '#333333',
    fontSize: 20,
  },
});


AppRegistry.registerComponent('Container', () => Container);