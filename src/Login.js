import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView,AppRegistry,KeyboardAvoidingView,Image } from "react-native";
import LoginForm from './LoginForm';



export default class Login extends Component {
    static navigationOptions={
        header: null
        
    }
  render() {
      const { navigation } = this.props
    return (
        <LoginForm  navigation={navigation} />
      );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#2c3e50',
  },
  loginContainer:{
      alignItems: 'center',
      flexGrow: 1,
      justifyContent: 'center'
  },
  logo: {
      width: 300,
      height: 100
  },
  title:{
      color: "#FFF",
      marginTop: 120,
      width: 180,
      textAlign: 'center',
      opacity: 0.9
  },

});