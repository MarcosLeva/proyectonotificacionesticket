import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView,AppRegistry , TouchableOpacity,TextInput,ToastAndroid} from "react-native";


export default class agregar extends Component {
    static navigationOptions = {
        title: ('AGREGAR'), 
          headerTitleStyle: {
            fontFamily: 'MuseoSansRounded-300',
            fontWeight: '300',
            justifyContent: 'space-between',
            textAlign: 'center'
          },
      }
  state = { tickets : [] }

  constructor(props){
    super(props)
    this.state= {
    }
  }


  componentDidMount() {
  }
  componentWillUnmount(){
  }
    onButtonPress (navigate)  {
        fetch('http://procesosiq.com:8001/tickets/insert',{
            method:'POST',
            headers: new Headers({
                'content-type':'application/json',
                'Accept': 'application/json'
            }),
            body: JSON.stringify({
                prioridad:  this.state.prioridad,
                cultivo:  this.state.cultivo,
                cliente:  this.state.cliente,
                usuario:  this.state.usuario,
                tipo:  this.state.tipo,
                tipo_tarea:  this.state.tipo_tarea,
                tema:  this.state.tema,
                estatus:  this.state.estatus,
                id  : this.state.id
            })
        })
        .then((response)=>response.json())
        .then((responseJson)=> {
            if(responseJson){
                console.log(responseJson)
                ToastAndroid.show('Guardado exitosamente!', ToastAndroid.SHORT);
                navigate('App')
            }
            
        })
        .catch((error)=> {
            console.log(error)
            ToastAndroid.show('Error!, No se ha podido guardar', ToastAndroid.SHORT);
        })
    };
    onChange= (name, value) => {
        this.setState({
            [name] : value
        })
    }
  _keyExtractor = (item, index) => item.id;

  render() {
      const inputs_props = {
        style : styles.input,
        autoCapitalize : "none", 
        autoCorrect : false, 
        placeholderTextColor : '#bbbbbb',
        selectionColor : '#0a9740'
      }
      const { navigate } = this.props.navigation;

    return (
        <ScrollView >
            <View style={styles.container}>
                <Text style={styles.title}>Prioridad:</Text>
                <View style={styles.container2}>
                    <TextInput 
                        {... inputs_props}
                        value={this.state.prioridad}
                        onChangeText = {(text)=>this.onChange("prioridad",text)}
                    />
                </View>
                <Text style={styles.title2}>Cultivo:</Text>
                <View style={styles.container2}>
                    <TextInput 
                        {... inputs_props} 
                        value={this.state.cultivo}
                        onChangeText = {(text)=>this.onChange("cultivo",text)}
                    />
                </View>
                <Text style={styles.title2}>Cliente:</Text>
                <View style={styles.container2}>
                    <TextInput 
                        {... inputs_props} 
                        value={this.state.cliente}
                        onChangeText = {(text)=>this.onChange("cliente",text)}
                    />
                </View>
                <Text style={styles.title2}>Usuario:</Text>
                <View style={styles.container2}>
                    <TextInput 
                        {... inputs_props} 
                        value={this.state.usuario}
                        onChangeText = {(text)=>this.onChange("usuario",text)}
                    />
                </View>
                <Text style={styles.title2}>Tipo:</Text>
                <View style={styles.container2}>
                    <TextInput 
                        {... inputs_props} 
                        value={this.state.tipo}
                        onChangeText = {(text)=>this.onChange("tipo",text)}
                    />
                </View>
                <Text style={styles.title2}>Tipo de tarea:</Text>
                <View style={styles.container2}>
                    <TextInput 
                        {... inputs_props} 
                        value={this.state.tipo_tarea}
                        onChangeText = {(text)=>this.onChange("tipo_tarea",text)}
                    />
                </View>
                <Text style={styles.title2}>Tema:</Text>
                <View style={styles.container2}>
                    <TextInput 
                        {... inputs_props} 
                        value={this.state.tema}
                        onChangeText = {(text)=>this.onChange("tema",text)}
                    />
                </View>
                <Text style={styles.title2}>Estatus:</Text>
                <View style={styles.container2}>
                    <TextInput
                        {... inputs_props} 
                        value={this.state.estatus}
                        onChangeText = {(text)=>this.onChange("estatus",text)}
                    />
                </View>
                <View style={styles.container2}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.onButtonPress(navigate)}}>
                        <Text  style={styles.buttonText}>Agregar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
    main: {
        justifyContent: 'center',
        alignItems: 'center',
      },
      buttonContainer:{
        backgroundColor: '#0a9740',
        marginTop:       20,
        marginBottom:    20,
        borderRadius:    8,
        height: 50,
        width: 300,
        alignItems:      'center',
        justifyContent:  'center',
    },
    buttonText:{
        textAlign:'center',
        color:    'white',
        fontSize:  20,
    }, 
    title: {
        paddingLeft: 55,
        paddingTop: 20,
        fontSize: 18,
     },
     title2: {
        paddingLeft: 55,
        fontSize: 18,
     },
     title3: {
        paddingLeft: 20,
        color:  '#0a9740',
        fontWeight: 'bold',
        fontSize: 26,
        width: 180,
        paddingTop: 40
     },
    container: {
        flex: 1,
       
     },
     container2: {
        alignItems:      'center',
        justifyContent:  'center',
     },
     input:{
        height: 40,
        marginBottom: 10,
        padding: 10,
        color:  '#0a9740',
        width: 350
        
    }
});

AppRegistry.registerComponent('agregar', () => agregar);