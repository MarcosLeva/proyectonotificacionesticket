import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView, AppRegistry, FlatList, TouchableOpacity, Image, TextInput} from "react-native";
import io from 'socket.io-client';

// crear socket conectado
const socket = io('http://procesosiq.com:8001'); 
// desconectar socket
socket.disconnect()
socket.on('connect', function(){console.log('conectado')});
socket.on('disconnect', function(){console.log('desconectado')});
function disconnectSocket(){
  socket.disconnect()
}
export default class App extends Component {
  static navigationOptions={
    header: null
  }
  state = { tickets : [] }
  state = { ticketsSearch : [] }
  constructor(props){
    super(props)
    this.connectSocket = this.connectSocket.bind(this)
    this.getTickets = this.getTickets.bind(this)
    this.state={
      buscar:'',
    }
  }


  
  connectSocket(){
    let self = this
    socket.connect()
    socket.on('new ticket', function({ referencia, titulo, mensaje, prioridad }){
      self.setState({
        newTicket : {
          referencia, titulo, mensaje, prioridad
        }
      })
    });
  }
  componentDidMount() {
    this.connectSocket()
    this.getTickets()
  }
  componentWillUnmount(){
    disconnectSocket()
  }
  onButtonPress (navigate,item)  {
    navigate('Update',{ticket:item})
  }
  onButtonPressAdd (navigate,item)  {
    navigate('Agregar')
  }
  onButtonPressSearch ()  {
    let self = this
    fetch('http://procesosiq.com:8001/tickets',{
        method:'GET',
        headers: new Headers({
            'content-type':'application/json',
            'Accept': 'application/json'
        }),
        body: JSON.stringify({
          id:  '2',
          
      })
    })
    .then((response)=>response.json())
    .then((responseJson)=> {
        if(responseJson){
           this.setState({
              tickets : responseJson
            },()=>{console.log(this.state.tickets)})
        }
    })
    .catch((error)=> {
        console.error(error)
    })
  };
  
  getTickets(){
    let self = this
    fetch('http://procesosiq.com:8001/tickets',{
        method:'GET',
        headers: new Headers({
            'content-type':'application/json',
            'Accept': 'application/json'
        }),
    })
    .then((response)=>response.json())
    .then((responseJson)=> {
        if(responseJson){
           this.setState({
              tickets : responseJson
            },()=>{console.log(this.state.tickets)})
        }
    })
    .catch((error)=> {
        console.error(error)
    })
  }
  Arreglo() {
    return this.state.tickets.map((tickets) => {
      return (
        <View><Text>{tickets}</Text></View>
      )
    })
}
_keyExtractor = (item, index) => item.id;
  render() {
    const { tickets } = this.state;
    const { navigate } = this.props.navigation;
    return (
      
      <View >
            <TouchableOpacity activeOpacity={0.5} onPress={() => {this.onButtonPressAdd(navigate)}} style={styles.TouchableOpacityStyle} >
              <Image source = {require('./floatButton.png')}
              style={styles.FloatingButtonStyle} />
            </TouchableOpacity>
            <View  style={styles.inputcenter}>
                <TextInput style = {styles.text} 
                  autoCapitalize="none" 
                  autoCorrect={false} 
                  placeholder='Prioridad, Estatus ó Cliente '
                  placeholderTextColor='#b0b0b0'
                  selectionColor='#0a9740'
                  value={this.state.id}
                />
                <TouchableOpacity  style = {styles.btnContainer}  onPress={() => {this.onButtonPressSearch()}}>
                  <Text style = {styles.btnText}>Buscar</Text>
                </TouchableOpacity>
            </View>
            <ScrollView>
              <View style={styles.main}>
              <FlatList
                data={this.state.tickets}
                keyExtractor={this._keyExtractor}
                renderItem={({item})=>
                <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.onButtonPress(navigate,item)}}>
                  <View style={styles.container}>
                    <Text style={styles.btnText}>
                      <Text style={styles.title}>Prioridad:</Text>  {item.prioridad}
                    </Text>
                    <Text style={styles.btnText}> 
                    <Text style={styles.title}>Cultivo:</Text>  {item.cultivo}
                    </Text>
                    <Text style={styles.btnText}>
                    <Text style={styles.title}>Cliente:</Text>  {item.cliente}
                    </Text>
                    <Text style={styles.btnText}>
                    <Text style={styles.title}>Usuario:</Text>   {item.usuario}
                    </Text>
                    <Text style={styles.btnText}>
                    <Text style={styles.title}> Tipo:</Text> {item.tipo}
                    </Text>
                    <Text style={styles.btnText}>
                    <Text style={styles.title}>Tipo de Tarea: </Text> {item.tipo_tarea}
                    </Text>
                    <Text style={styles.btnText}>
                    <Text style={styles.title}> Tema: </Text> {item.tema}
                    </Text>
                    <Text style={styles.btnText}>
                    <Text style={styles.title}> Estatus:</Text>  {item.estatus}
                    </Text>
                  </View>
                </TouchableOpacity>
                }
              />  
              </View>
          </ScrollView>
        </View>
    );
  }
}
const styles = StyleSheet.create({
  main: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: '#0a9740',
    fontSize:  18,
    fontWeight: 'bold',
    width: 400
  },
  buttonContainer:{
    backgroundColor: '#3d3d3d',
    paddingVertical: 20,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: 400,
    height: 240,
    borderRadius: 8,
  },
  container: {
    padding: 20,
  },
  MainContainer: {
    alignItems: 'center',
    backgroundColor : '#F5F5F5'
  },
  text:{
    height: 40,
    padding: 10,
    color:  '#0a9740',
    width: 280,
    fontSize: 20,
    marginTop: 25,
  },
  inputcenter:{
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  buttonText:{
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
  }, 
  TouchableOpacityStyle:{
    position: 'absolute',
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 90,
    zIndex: 1
  },
  btnText:{
      textAlign:'center',
      color:    '#b0b0b0',
    },
  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 70,
    height: 70,
  },
  btnContainer:{
    backgroundColor: '#0a9740',
    paddingVertical: 5,
    padding: 20,
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 8,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  btnText:{
    textAlign:'center',
    color: 'white',
    fontSize:  20,
  },
});

AppRegistry.registerComponent('App', () => App);