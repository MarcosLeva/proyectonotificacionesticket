import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity,Image, StyleSheet, ToastAndroid} from 'react-native';


class LoginForm extends Component {

    constructor(props){
        super(props);
        this.state={
            user:'',
            password:'',
        }
    
    }

    onButtonPress (navigate)  {
        fetch('http://procesosiq.com:8001/login',{
            method:'POST',
            headers: new Headers({
                'content-type':'application/json',
                'Accept': 'application/json'
            }),
            body: JSON.stringify({
                user:  this.state.user,
                password:this.state.password
                
            })
        })
        .then((response)=>response.json())
        .then((responseJson)=> {
            if(responseJson){
                console.log(responseJson)
            if(responseJson.username=this.state.user ) {
                navigate('App')
                ToastAndroid.show('Bienvenido, '+ this.state.user + "!", ToastAndroid.SHORT);
            }
            }
        })
        .catch((error)=> {
            ToastAndroid.show('Usuario ó contraseña incorrectos', ToastAndroid.SHORT);

        })
    };
    
    actUser=(valor)=> {
        this.setState({
            user:valor
        },()=>{global.user=valor})
    }

    actPassword=(valor)=> {
        this.setState({
            password:valor
        },()=>{global.password=valor})
    }


    render() {
        const { navigate } = this.props.navigation;
        return (
<       View style = {styles.container}>
            <Image 
            source = {require('./ProcesosIQ.png')}
            />
            <TextInput
                autoCapitalize="none" 
                onSubmitEditing={() => this.passwordInput.focus()} 
                autoCorrect={false} 
                returnKeyType="next" 
                placeholder='Usuario' 
                placeholderTextColor='#bbbbbb'
                selectionColor='#0a9740'
                value={this.state.user}
                onChangeText={this.actUser}
                style = {styles.text}
            />
            <TextInput
                returnKeyType="go" ref={(input)=> this.passwordInput = input} 
                placeholder='Contraseña' 
                placeholderTextColor='#bbbbbb' tu
                secureTextEntry
                value={this.state.password}
                onChangeText={this.actPassword}
                selectionColor='#0a9740'
                style = {styles.text}
            />
            
            <TouchableOpacity  style = {styles.btnContainer}  onPress={() => {this.onButtonPress(navigate)}}>
            <Text style = {styles.btnText}>Ingresar</Text>
            </TouchableOpacity>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#fff',
    alignItems:      'center',
    justifyContent:  'center',
  },
  logo: {
    width: 300,
    height: 100
  },
  text:{
    height: 40,
    width: 350,
    margin: 15,
    // se colorean los bordes por individual porque react se fresea
    borderBottomColor: 'white',
    borderTopColor:    'white',
    borderLeftColor:   'white',
    borderRightColor:  'white',
    borderWidth: 1,
    color:  '#0a9740',
    fontSize: 18
  },
  btnContainer:{
    backgroundColor: '#0a9740',
    paddingVertical: 5,
    padding:         100,
    marginTop:       20,
    marginBottom:    20,
    borderRadius:    8,
    height: 50,
    alignItems:      'center',
    justifyContent:  'center',
    
  },
  btnText:{
    textAlign:'center',
    color:    'white',
    fontSize:  20,
  },

  
});

export default LoginForm;
