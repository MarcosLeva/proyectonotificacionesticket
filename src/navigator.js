import {createStackNavigator,NavigationActions, navigationOptions } from 'react-navigation';
import App from './App';
import LoginForm from './Login';
import Update from './Update';
import Agregar from './agregar';




const navigator = createStackNavigator({
    LoginForm: { screen: LoginForm },
    App: { screen: App },
    Update: { screen: Update },
    Agregar: { screen: Agregar }
},{ 
        headerMode: 'screen' 
      }
);

  
export default navigator;