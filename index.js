import { AppRegistry } from 'react-native';
import navigator from './src/navigator'

import SplashScreen from "react-native-splash-screen";
SplashScreen.hide();

AppRegistry.registerComponent('NotificacionesTicket', () => navigator);
